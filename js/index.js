$(function () {
    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
    $('.carousel').carousel({
      interval: 2000
    });
    $('#oneModal').on('show.bs.modal', function (e) {
      console.log('el modal se esta mostrando.');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
    });
    $('#oneModal').on('shown.bs.modal', function (e) {
      console.log('el modal se mostro.');
    });
    $('#oneModal').on('hide.bs.modal', function (e) {
      console.log('el modal se oculta.');
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
      $('#contactoBtn').prop('disabled', false);
    });
    $('#oneModal').on('hidden.bs.modal', function (e) {
      console.log('el modal se oculto.');
    });
});